<?php

 * @author Ignacio Segura, "Nachenko" <nacho@pensamientosdivergentes.net>
 * @package module_webform
*/

// Webform annotate custom functions
// These are reusable functions used by some in the module file
// All hooks, callbacks etc are in hte module file.

// Given a nodeid ($nid) and a reply id ($sid), returns the user that replied this form
function webform_annotate_whoisthisform ($nid, $sid) {
	return (db_result(db_query("SELECT uid FROM {webform_submissions} WHERE nid = $nid AND sid = $sid")));
}

// Has user access to see someone's received annotations?
function webform_annotate_hasaccess_someone_stuff($user_being_browsed) {
	$hasaccess = false;
	global $user;
	if (user_access("view annotations to forms")) {
		$hasaccess = true;
	} elseif (user_access("view annotations to own forms") && $user->uid == $user_being_browsed) {
		$hasaccess = true;
	}
	return $hasaccess;
}

/**
 * This function creates a table for all submissions.
 * @param $nid
 *   The node ID of the node to show results for.
 */
function _webform_annotate_results_table($nid) {
  include_once(drupal_get_path('module', 'webform') ."/webform.inc");
  // Load Components.
  _webform_load_components();

  // Get all the component cid and names for the node.
  $query = 'SELECT cid, name, type, extra FROM {webform_component} WHERE nid = %d ORDER BY weight, name';
  $res = db_query($query, $nid);
  while ($component = db_fetch_array($res)) {
    $components[] = $component;
  }

  // Get all the submissions for the node.
  $header = theme('webform_results_table_header', $nid);
  $submissions = _webform_annotate_fetch_submissions($nid, $header);

  return theme('webform_annotate_results_table', $nid, $components, $submissions);
}


function _webform_annotate_results_download($nid) {
  include_once(drupal_get_path('module', 'webform') ."/webform.inc");

/*
 * The CSV requires that the data be presented in a flat file.  In order
 * to maximize useability to the Excel community and minimize subsequent
 * stats or spreadsheet programming this program extracts data from the
 * various records for a given session and presents them as a single file
 * where each row represents a single record.
 
 * NOTICE: This function is taken from webform.module, then modified.

 */
  $node = node_load(array('nid' => $nid));

  $file_name = tempnam(variable_get('file_directory_temp', FILE_DIRECTORY_TEMP), 'webform');
  $handle = @fopen($file_name, 'w'); // The @ suppresses errors.
  $header[0] .= $node->title .",,,,";
  $header[1] .= "Submission Details,,,,";
  $header[2] .= "Serial,SID,Time,UID,Username";
  // Compile header information.
  _webform_load_components(); // Load all components.
  if (is_array($node->webformcomponents)) {
    foreach ($node->webformcomponents as $cid => $component) {
      $csv_header_function   = "_webform_csv_headers_". $component['type'];
      if (function_exists($csv_header_function)) {
        // Let each component determine its headers.
        $component_header = $csv_header_function($component);
        $header[0] .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $component_header[0]) .'"';
        $header[1] .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $component_header[1]) .'"';
        $header[2] .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $component_header[2]) .'"';
      }
    }
  }
	$header[2] .= ',"'.t("Rating").'","'.t('Note').'"';

  // Write header information.
  $file_record = $header[0] ."\n". $header[1] ."\n". $header[2] ."\n";
  @fwrite($handle, $file_record);

  // Get all the submissions for the node.
  $submissions = _webform_annotate_fetch_submissions($nid);

  // Generate a row for each submission.
  $rowcount = 0;
  foreach ($submissions as $sid => $submission) {
    $row = ++$rowcount .",". $sid .",\"". format_date($submission->submitted, 'small') ."\",". $submission->uid .",\"". $submission->name ."\"";
    foreach ($node->webformcomponents as $cid => $component) {
      $csv_data_function   = "_webform_csv_data_". $component['type'];
      if (function_exists($csv_data_function)) {
        // Let each component add its data.
        $row .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $csv_data_function($submission->data[$cid], $component)) .'"';
      }
    }
		$row .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $submission->rating) .'"';
		$row .= ',"'. str_replace(array('"', '\,'), array('""', '","'), $submission->note) .'"';
    // Write data from submissions.
    @fwrite($handle, $row ."\n");
  }
  // Close the file.
  @fclose($handle);

  drupal_set_header("Content-type: text/csv; charset=utf-8");
  drupal_set_header("Content-Disposition: attachment; filename=". preg_replace('/\.$/', '', str_replace(' ', '_', $node->title)) .".csv");

  @readfile($file_name);  // The @ makes it silent.
  @unlink($file_name);  // Clean up, the @ makes it silent.
  exit(0);
}
/**
 * Return all the submissions for a particular node.
 * @param $nid
 *   The node ID for which submissions are being fetched.
 * @param $header
 *   If the results of this fetch will be used in a sortable table, pass the
 *   array header of the table.
 * NOTICE: This function is taken from webform module, then modified
 */
function _webform_annotate_fetch_submissions($nid, $header = NULL) {

  $query = 'SELECT s.sid, s.uid, s.submitted, s.remote_addr, sd.cid, sd.no, sd.data, u.name, u.mail, u.status, a.rating, a.note '.
           'FROM {webform_submissions} s '.
           'LEFT JOIN {webform_submitted_data} sd ON sd.sid = s.sid '.
           'LEFT JOIN {users} u ON u.uid = s.uid '.
					 'LEFT JOIN {webform_annotate} a ON s.sid = a.sid '.
           'WHERE sd.nid = %d';

  if (is_array($header)) {
    $query .= tablesort_sql($header);
  }

  $res = db_query($query, $nid);
  $submissions = array();
  $previous = array();

  // Outer loop: iterate for each submission.
  while ($row = db_fetch_object($res)) {
    if ($row->sid != $previous) {
      $submissions[$row->sid]->sid = $row->sid;
      $submissions[$row->sid]->submitted = $row->submitted;
      $submissions[$row->sid]->remote_addr = $row->remote_addr;
      $submissions[$row->sid]->uid = $row->uid;
      $submissions[$row->sid]->name = $row->name;
      $submissions[$row->sid]->status = $row->status;
      $submissions[$row->sid]->rating = $row->rating;
      $submissions[$row->sid]->note = $row->note;
    }
    $submissions[$row->sid]->data[$row->cid]['value'][$row->no] = $row->data;
    $previous = $row->sid;
  }
  return $submissions;
}

/**
	* Webform annotate submission forms
	*/
function _webform_annotate_form ($node) {
	$form = array ();
	$form['#redirect'] = array (drupal_get_path_alias("node/".$node->nid), "sid=".$node->sid);
	$form['webform_annotate'] = array (
		'#type' => 'fieldset',
		'#title' => t("Notes and rating")
	);
	$form['webform_annotate']['nid'] = array(
		'#type' => 'value',
		'#value' => $node->nid
	);
	$form['webform_annotate']['sid'] = array(
		'#type' => 'value',
		'#value' => $node->sid
	);
	if ($node->annotate) {
		$form['webform_annotate']['annotate_id'] = array(
			'#type' => 'value',
			'#value' => $node->annotate->id
		);
	}
	// Get general part of the annotation form
	$form = _webform_annotate_general_annotation_form ($form, $node);
	// Textboxes for every item
	$form = _webform_annotate_items_annotation_form ($form, $node);
	
	$form['webform_annotate']['submit'] = array(
		'#type' => "submit",
		'#value' => t("Update")
	);
	return $form;
}

// Webform annotate form's general part
function _webform_annotate_general_annotation_form ($form, $node) {
	$usercanrate = variable_get ('webform_annotate_rating', false);
	if (user_access("rate other users' replies to webforms") == 1 && $usercanrate['enable'] === 'enable') {
		$form['webform_annotate']['rating'] = array(
			'#type' => 'textfield',
			'#title' => t("Rating on this form"),
			'#required' => false,
			'#maxlength' => 4,
			'#size' => 4,
			'#default_value' => $node->annotate->rating,
			'#description' => t("Rate this form 0-10. Decimal values allowed.")
		);
	}
	if (user_access("annotate other users' replies to webforms") == 1) {
		$form['webform_annotate']['note'] = array(
			'#type' => 'textarea',
			'#title' => t("Annotations on this form"),
			'#required' => false,
			'#default_value' => $node->annotate->note,
			'#description' => t("Write your notes about this form. Only you and the user will be able to see them.")
		);
	}
	return $form;
}

// Webform annotate form's every item's part
function _webform_annotate_items_annotation_form ($form, $node) {
	if (user_access("annotate other users' replies to webforms") == 1) {
		$submission = _webform_fetch_submission ($node->sid, $node->nid);
		$annotable_fields = array ('textarea', 'textfield');
		foreach ($submission['data'] as $key => $value) {
			$item_type = db_result(db_query("SELECT type FROM {webform_component} WHERE nid = %d AND cid = '%s'", $node->nid, $key));
			if (in_array($item_type, $annotable_fields)) { 
				$form['webform_annotate'][$key] = array(
				'#type' => 'textarea',
				'#title' => t('Notes on:').' '.$node->webformcomponents[$key]['name'],
				'#required' => false,
				'#rows' => 3,
				'#default_value' => $node->annotate->items[$key],
				);
			}
		}
		unset ($submission);
	}
	return $form;
}

// Helper function to prepare item annotations to store in database
// Just wanted to make main function shorter
function _webform_annotate_prepare_items_array ($form_values) {
	unset ($form_values['nid']);
	unset ($form_values['sid']);
	unset ($form_values['annotate_id']);
	unset ($form_values['submit']);
	unset ($form_values['rating']);
	unset ($form_values['note']);
	unset ($form_values['op']);
	unset ($form_values['form_token']);
	unset ($form_values['form_id']);
	return $form_values;
}

// Helper function stores annotation items
function _webform_annotate_save_items($item_notes, $form_values) {
	foreach ($item_notes as $cid => $text) {
		$trimmed_text = trim ($text);
		$result = db_query("UPDATE {webform_annotate_items} SET text = '%s' WHERE cid = '%s' AND nid = %d AND sid = %d", $trimmed_text, $cid, $form_values['nid'], $form_values['sid'] );
		if (db_affected_rows() == 0 || !$result) {
			db_query("INSERT INTO {webform_annotate_items} (nid, sid, cid, text) VALUES (%d, %d, '%s', '%s')", $form_values['nid'], $form_values['sid'], $cid, $trimmed_text );
		}
	}
}
?>